FROM alpine:latest
ENV FLASK_APP=run.py FLASK_CONFIG=production FLASK_PORT=5000 NODE_ENV=production
RUN apk add --no-cache python3 && \
    python3 -m ensurepip && \
    rm -r /usr/lib/python*/ensurepip && \
    pip3 install --upgrade pip setuptools && \
    if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi && \
    if [[ ! -e /usr/bin/python ]]; then ln -sf /usr/bin/python3 /usr/bin/python; fi && \
    rm -r /root/.cache
RUN apk add --update --no-cache python3-dev nodejs py-mysqldb mysql-dev gcc g++ make git bash
RUN npm install --only=production -g bower
COPY . /parasite
WORKDIR /parasite
RUN chmod +x boot.sh
RUN pip3 install -r requirements.txt
RUN cd app/static && npm install --only=production && cd ../../
RUN cd app/static && bower --allow-root -P install && cd ../../
EXPOSE 5000
CMD ['python3', 'run.py']
