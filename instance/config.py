# Import OS to set default folder
import os
from redis import Redis

# Define the application directory
BASE_DIR = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    # Statement for enabling the development environment
    DEBUG = True
    TESTING = True

    # Define the database - we are working with
    # SQLite on development
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASE_DIR, 'app.db')
    DATABASE_CONNECT_OPTIONS = {}

    # Statement for SQLALCHEMY to track development modifications
    # Adds significantly overhead on server
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # Application threads. A common general assumption is
    # using 2 per available processor cores - to handle
    # incoming requests using one and performing background
    # operations using the other.
    THREADS_PER_PAGE = 2

    # Session cookies last 10 minutes
    REMEMBER_COOKIE_DURATION = 600

    # Enable protection agains *Cross-site Request Forgery (CSRF)*
    CSRF_ENABLED = True

    # Reload templates
    TEMPLATES_AUTO_RELOAD = True

    # Use a secure, unique and absolutely secret key for
    # signing the data.
    CSRF_SESSION_KEY = "16a2fd4677fe58414801b6001d6ddec0d248658f90e1fe2c"

    # Secret key for signing cookies
    SECRET_KEY = "15b2777a8c56691913c54a157ff82b12212a4924f99440be"

    # WTF CSRF Secret Key
    WTF_CSRF_SECRET_KEY = "376f039302c9dedc8b3dfd8186b6de498804f47333cf51f6"

    # Recaptcha Public Key
    RECAPTCHA_PUBLIC_KEY = "e1242f006f810e549466b21a31c5c60b20070fad4a34423d"

    # Redis database url
    REDIS_HOST = 'localhost'
    REDIS_PORT = '6379'
    REDIS_DB = '0'

    # Session type for user session
    SESSION_TYPE = 'redis'

class ProductionConfig(Config):

    # Statement for enabling the production environment
    DEBUG = False
    TESTING = False

    # Define the database - we are working with
    # SQLite on development
    SQLALCHEMY_DATABASE_URI = 'mysql://parasite:323956482@db/parasite_prod'
    DATABASE_CONNECT_OPTIONS = {}

    # Statement for SQLALCHEMY to track development modifications
    # Adds significantly overhead on server
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # Application threads. A common general assumption is
    # using 2 per available processor cores - to handle
    # incoming requests using one and performing background
    # operations using the other.
    THREADS_PER_PAGE = 8

    # Enable protection agains *Cross-site Request Forgery (CSRF)*
    CSRF_ENABLED     = True

    # Redis database configurations
    REDIS_HOST = 'redis'
    REDIS_PORT = '6379'
    REDIS_DB = '0'

    # Session type for user session
    SESSION_TYPE = 'redis'
    SESSION_REDIS = Redis(host='redis', port='6379', db='0')

class DevelopmentConfig(Config):

    # Statement for enabling the development environment
    DEBUG = True
    TESTING = False
    SQLALCHEMY_ECHO = True

    # MYSQL Url to parasite database
    SQLALCHEMY_DATABASE_URI = 'mysql://parasite_admin:pwadmin@localhost/parasite_dev'


class TestingConfig(Config):

    # Statement for enabling the testing environment
    DEBUG = True
    TESTING = True

    # Define the database - we are working with
    # SQLite on development
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASE_DIR, 'app_testing.db')
    DATABASE_CONNECT_OPTIONS = {}

app_config = {
    'development': DevelopmentConfig,
    'production': ProductionConfig,
    'testing': TestingConfig
}
