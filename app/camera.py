import cv2
from threading import Thread
from flask_socketio import emit

class VideoCamera(Thread):

    def __init__(self, threadID, name, stop_event):
        Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.stop_event = stop_event

        self.video = cv2.VideoCapture(0)
        success, image = self.video.read()
        ret, jpeg = cv2.imencode('.jpg', image)
        self.img = jpeg.tobytes()

    def run(self):
        while not self.stop_event.is_set():
            self.update_frame()
            emit('image_feed', {'data': self.img})

    def __del__(self):
        self.video.release()

    def stop(self):
        self.stop_event.set()

    def is_stopped(self):
        return self.stop_event.is_set()

    def update_frame(self):
        success, image = self.video.read()
        if (success):
            ret, jpeg = cv2.imencode('.jpg', image)
            self.img = jpeg.tobytes()