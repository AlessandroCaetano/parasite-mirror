# Import flask dependencies
from flask import request, make_response, jsonify

# Import flask login dependencies
from flask_login import current_user, login_required

# Import Gathering blueprint configuration
from . import api

# Import app configurations
from .. import db

# Import module models (i.e. Sample)
from ..models import  User, Image, ImageSchema, Slide, SlideSchema, \
    Sample, SampleSchema

# Set the route and accepted methods
@api.route('/users/signup', methods=['POST'])
def signup_api():
    user = User.query.filter_by(username=request.form['username']).first()
    if not user:
        try:
            post_data = request.form
            print(post_data)
            username = post_data['username']
            passwd = post_data['passwd']
            user = User(username=username, password=passwd)
            db.session.add(user)
            db.session.commit()
            response = {
                'message': 'User created successfully. Please sign in now'
            }
            return make_response(jsonify(response)), 201
        except Exception as e:
            response = {
                'message': str(e)
            }
            return make_response(jsonify(response)), 401
    else:
        response = {
            'message': 'User already exists. Please sign in.'
        }
        return make_response(jsonify(response)), 202

@api.route('/users/signin', methods=['POST'])
def signin_api():
    user = User.query.filter_by(username=request.form['username']).first()
    if user is not None and user.verify_password(request.form['passwd']):
        try:
            access_token = user.generate_token(user.id)
            print(access_token)
            if access_token:
                response = {
                    'message': 'User signed in successfully.',
                    'access_token': access_token.decode()
                }
                return make_response(jsonify(response)), 200
            else:
                response = {
                    'message': 'Invalid username or password. Try again.'
                }
                return make_response(jsonify(response)), 401
        except Exception as e:
            response = {
                'message': str(e)
            }
            return make_response(jsonify(response)), 500

@api.route('/images/', methods=['GET'])
def images_api():
    auth_header = request.headers.get('Authorization')
    access_token = auth_header.split(" ")[1]
    if access_token:
        # Attempt to decode the token and get the User ID
        user_id = User.decode_token(access_token)
        if not isinstance(user_id, str):
            # Go ahead and handle the request, the user is authenticated
            if request.method == 'GET':
                images = Image.query.all()
                image_schema = ImageSchema(many=True)
                output = image_schema.dump(images).data
                return make_response(jsonify({'images' : output})), 200
        else:
            message = user_id
            response = {
                'message': message
            }
            return make_response(jsonify(response)), 401

@api.route('/slides/', methods=['GET'])
def slides_api():
    auth_header = request.headers.get('Authorization')
    access_token = auth_header.split(" ")[1]
    if access_token:
        # Attempt to decode the token and get the User ID
        user_id = User.decode_token(access_token)
        if not isinstance(user_id, str):
            # Go ahead and handle the request, the user is authenticated
            if request.method == 'GET':
                slides = Slide.query.all()
                slide_schema = SlideSchema(many=True)
                output = slide_schema.dump(images).data
                return make_response(jsonify({'images' : output})), 200
        else:
            message = user_id
            response = {
                'message': message
            }
            return make_response(jsonify(response)), 401

@api.route('/samples/', methods=['GET'])
def sample_api():
    auth_header = request.headers.get('Authorization')
    access_token = auth_header.split(" ")[1]
    if access_token:
        # Attempt to decode the token and get the User ID
        user_id = User.decode_token(access_token)
        if not isinstance(user_id, str):
            # Go ahead and handle the request, the user is authenticated
            if request.method == 'GET':
                samples = Samples.query.all()
                sample_schema = SampleSchema(many=True)
                output = sample_schema.dump(images).data
                return make_response(jsonify({'images' : output})), 200
        else:
            message = user_id
            response = {
                'message': message
            }
            return make_response(jsonify(response)), 401

