# Import Flask Blueprint to set api blueprint
from flask import Blueprint

# Define the blueprint: 'api', set its url prefix: app.url/api
api = Blueprint('api', __name__, url_prefix='/api')

from . import views
