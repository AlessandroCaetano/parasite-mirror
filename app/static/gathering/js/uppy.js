var full = location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
const uppy = Uppy.Core({
    debug: true,
    autoProceed: false,
    restrictions: {
        maxFileSize: 1000000,
        maxNumberOfFiles: 1,
        minNumberOfFiles: 1,
        note: 'Images only, 1 file, up to 1 MB',
        allowedFileTypes: ['image/*']
    }
})
    .use(
        Uppy.Dashboard, {
            trigger: 'uppymodalopenerbtn',
            inline: true,
            target: '.dashboardcontainer',
            replaceTargetContent: true,
            showProgressDetails: true,
        }
    )
    .use(
        Uppy.Webcam, {
            target: Uppy.Dashboard,
            countdown: false,
            modes:[
                'picture'
            ],
            mirror: true,
            facingMode: 'user'

        }
    )
    .use(
        Uppy.Tus, {
            endpoint: '//0.0.0.0:1080/files/'
        }
    )
    .on('complete', (result) => {
        console.log('successful files:', result.successful[0].uploadURL)
        document.getElementById('image_url').value = result.successful[0].uploadURL
        document.getElementById('form-sample').submit()
    })
    .run()
