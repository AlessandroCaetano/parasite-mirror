 let namespace = '/images/snapshot'
 var socket = io.connect(location.protocol + '//' + document.domain + ':' + location.port + namespace);
    $(document).ready(function(){
        //connect to the socket server.
        socket.on('connect', function() {
            socket.emit('connected', {data: 'Client Connected'});   });
        socket.on('image_feed', function(msg) {
            $('#image').append('<img src=' + msg.data + 'class="img-fluid">');  });
        socket.on('disconnect', function() {
            socket.emit('disconnected', {data: 'Client Disconnected'});
            socket.disconnect()
        });
    });
    function set_cam() {
        socket.emit('set_cam')
    }
    function unset_cam() {
        socket.emit('unset_cam')
    }