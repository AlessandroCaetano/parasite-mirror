function open_cam() {
    Webcam.attach( '#my_camera' );
};
function take_snapshot() {
    Webcam.snap( function(data_uri) {
        document.getElementById('my_result').innerHTML = '<img class="mw-100 w-100 mh-100 h-100" src="'+data_uri+'"/>';
    } );
};
function close_cam() {
    Webcam.reset();
};
