# Import the database object (db) from the main application module

import jwt

from datetime import datetime, timedelta

from app import db, lm, ma

from flask import current_app

from flask_login import UserMixin

from werkzeug.security import generate_password_hash, \
    check_password_hash

# Define a base model for other database tables to inherit
class Base(db.Model):

    __abstract__  = True

    id            = db.Column(db.Integer, autoincrement=True, primary_key=True,
                              nullable=False, index=True)
    date_created  = db.Column(db.DateTime,  default=db.func.current_timestamp(),
                              nullable=False, index=True)
    date_modified = db.Column(db.DateTime,  default=db.func.current_timestamp(),
                              onupdate=db.func.current_timestamp(),
                              nullable=False, index=True)
# Define a User model
class User(Base, UserMixin):

    __tablename__ = 'users'


    username = db.Column(db.String(60), unique=True, nullable=False, index=True)
    hashed_password = db.Column(db.String(255), nullable=False, index=True)
    authenticated = db.Column(db.Boolean, nullable=False, default=False, index=True)

    def generate_token(self, id):
        """
        Generates access token for api requests
        """
        try:
            payload = {
                'exp': datetime.utcnow() + timedelta(minutes=5),
                'iat': datetime.utcnow(),
                'sub': id
            }
            jwt_string = jwt.encode(
                payload,
                current_app.config.get('SECRET_KEY'),
                algorithm='HS256'
            )
            return jwt_string

        except Exception as e:
            return string(e)

    @staticmethod
    def decode_token(token):
        """
        Decodes access token from Authorization Bearer
        """
        try:
            # try to decode token with secret key
            payload = jwt.decode(token, current_app.config.get('SECRET_KEY'))
            return payload['sub']
        except jwt.ExpiredSignatureError:
            # token expired after 5 mins
            return "Expired token. Please request a new token."
        except jwt.InvalidTokenError:
            # token not valid
            return "Invalid token. Please register or login."

    @property
    def password(self):
        """
        Prevent pasword from being accessed
        """
        raise AttributeError('password is not a readable attribute.')

    @password.setter
    def password(self, password):
        self.hashed_password = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.hashed_password, password)

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.username)

    def __repr__(self):
        return '<User %r>' % (self.username)


@lm.user_loader
def load_user(username):
    return User.query.filter_by(username=username).first()


# Define a Image model
class Image(Base):

    __tablename__ = 'images'

    # Capture date
    capture_date  = db.Column(db.Date,  nullable=False, index=True)

    # Zoom value
    zoom     = db.Column(db.Integer, nullable=False, index=True)

    # Filename
    filename = db.Column(db.String(255), nullable=False, index=True)

    # Image url
    url = db.Column(db.String(255), nullable=False, index=True)

    # Relationship attributes
    slide_id = db.Column(db.Integer, db.ForeignKey('slides.id'))
    slide = db.relationship('Slide')


    # New instance instantiation procedure
    def __init__(self, capture_date, zoom, filename, url):

        self.capture_date = capture_date
        self.zoom = zoom
        self.filename = filename
        self.url = url

# Define a Equipment model
class Equipment(Base):

    __tablename__ = 'equipments'

    # Code value
    code     = db.Column(db.Integer, nullable=False, index=True)

    # New instance instantiation procedure
    def __init__(self, code):

        self.code = code

# Define a Allocation model
class Allocation(Base):

    __tablename__ = 'allocations'

    # Initial and Final date values
    date_initial  = db.Column(db.Date, nullable=False, index=True)
    date_final = db.Column(db.Date,  nullable=False, index=True)

    # New instance instantiation procedure
    def __init__(self, date_initial, date_final):

        self.date_final = date_final
        self.date_initial = date_initial

# Define a Slide model
class Slide(Base):

    __tablename__ = 'slides'

    # Initial and Final date values
    creation_date  = db.Column(db.Date, nullable=False, index=True)
    technique = db.Column(db.String(60),  nullable=False, index=True)

    # Relationship attributes
    sample_id = db.Column(db.Integer, db.ForeignKey('samples.id'))
    sample = db.relationship('Sample')

    images = db.relationship('Image', backref='slide_images', lazy='dynamic')

    # New instance instantiation procedure
    def __init__(self, creation_date, technique, images = images):

        self.creation_date = creation_date
        self.technique = technique
        self.images = images if images is not None else []

# Define a Sample model
class Sample(Base):
    __tablename__ = 'samples'

    # Sample attributes
    identification = db.Column(db.String(60),  nullable=False, index=True)
    collected_date = db.Column(db.Date, nullable=False, index=True)
    country = db.Column(db.String(60),  nullable=False, index=True)
    city = db.Column(db.String(60),  nullable=False, index=True)
    district = db.Column(db.String(60),  nullable=False, index=True)
    number = db.Column(db.Integer, nullable=False, index=True)

    # Relation attributes
    slides = db.relationship('Slide', backref='sample_slides', lazy='dynamic')

    # New instance instantiation procedure
    def __init__(self, identification, collected_date, country, city, district,\
                 number, slides):

        self.identification = identification
        self.collected_date = collected_date
        self.country = country
        self.city = city
        self.district = district
        self.number = number
        self.slides = slides if slides is not None else []

# Define Image Api Schema
class ImageSchema(ma.ModelSchema):
    class Meta:
        model = Image

# Define Slide Api Schema
class SlideSchema(ma.ModelSchema):
    class Meta:
        model = Slide

# Define Sample Api Schema
class SampleSchema(ma.ModelSchema):
    class Meta:
        model = Sample