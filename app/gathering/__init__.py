# Import Flask Blueprint to set gathering blueprint
from flask import Blueprint

# Define the blueprint: 'gathering', set its url prefix: app.url/gathering
gathering = Blueprint('gathering', __name__, url_prefix='/gathering', template_folder='../templates/gathering')

from . import views