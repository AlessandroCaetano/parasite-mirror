# Import Form
from flask_wtf import FlaskForm

# Import Form elements such as TextField and BooleanField (optional)
from wtforms import StringField, DateField, SelectField, SubmitField \
                    , FileField, BooleanField

# Import Form validators
from wtforms.validators import DataRequired

# Define Sample form  (WTForms)
class SampleForm(FlaskForm):
    identification = StringField('Identification', validators=[DataRequired(message='This field is obligatory')])
    collected_date = DateField('Collected_date', format='%m/%d/%Y', validators=[DataRequired(message='This field is obligatory')])
    country = SelectField('Country', choices=[('Brazil', 'Brazil'), ('South Africa', 'South Africa')], validators=[DataRequired(message='This field is obligatory')] )
    city = StringField('City', validators=[DataRequired(message='This field is obligatory')])
    district = StringField('District', validators=[DataRequired(message='This field is obligatory')])
    number = StringField('Number', validators=[DataRequired(message='This field is obligatory')])
    submit = SubmitField('Save')

# Define Slide form  (WTForms)
class SlideForm(FlaskForm):
    creation_date = DateField('Creation_Date', format='%m/%d/%Y')
    technique = StringField('Technique', validators=[DataRequired(message='This field is obligatory')])
    images  = BooleanField('Images')
    submit = SubmitField('Save')

# Define Image form  (WTForms)
class ImageForm(FlaskForm):
    filename = StringField('Filename')
    cdate = DateField('CDate', format='%m/%d/%Y')
    zoom = StringField('Zoom')
    image_url = StringField('Image_url')
    submit = SubmitField('Save')