# Import flask dependencies
from flask import request, render_template, flash, redirect, url_for

# Import flask login dependencies
from flask_login import current_user, login_required

# Import Gathering blueprint configuration
from . import gathering

# Import app configurations
from .. import db

# Import module forms
from .forms import SampleForm, SlideForm, ImageForm

# Import module models (i.e. Sample)
from ..models import  Sample, Slide, Image

# Set the route and accepted methods

@gathering.route('/')
@login_required
def dashboard():
    user = current_user
    samples = Sample.query.count()
    slides = Slide.query.count()
    images = Image.query.count()
    return render_template('index.html', title='ParasiteWatch Dashboard', \
                           user=user, samples = samples, slides = slides, \
                           images = images)

@gathering.route('/samples', methods=['GET'])
@login_required
def samples():
    user = current_user
    form = SampleForm()
    samples = Sample.query.all()
    return render_template('sample/sample_index.html', title='ParasiteWatch Dashboard', user=user, form=form, samples=samples)

@gathering.route('/samples/new', methods=['GET', 'POST'])
@login_required
def samples_new():
    user = current_user
    form = SampleForm()
    slides = Slide.query.filter_by(sample_id=None)
    if request.method == 'POST':
        if form.validate_on_submit():
            selected_slides = request.form.getlist('slides')
            slide_objects = []
            for slide_id in selected_slides:
                slide_objects.append(Slide.query.filter_by(id=slide_id).first())
            sample = Sample(collected_date = form.collected_date.data, identification = form.identification.data,\
                            country = form.country.data, city = form.city.data, \
                            district = form.district.data, number = form.district.data, \
                            slides = slide_objects)
            db.session.add(sample)
            db.session.commit()
            db.session.flush()
            flash('Sample created successfully', category = 'success')
            return redirect(url_for('gathering.samples'))
        flash('Error sending form data', category = 'danger')
        return redirect(url_for('gathering.samples_new'))
    if request.method == 'GET':
        return render_template('sample/sample_new.html', title = 'ParasiteWatch Slides', user = user, form = form, slides = slides)


@gathering.route('/samples/destroy/<sample_id>', methods=['POST'])
@login_required
def samples_destroy(sample_id):
    user = current_user
    form = SampleForm()
    if request.method == 'POST':
        sample = Sample.query.filter_by(id = sample_id).first()
        db.session.delete(sample)
        db.session.commit()
        db.session.flush()
        flash('Sample deleted successfully', category='success')
        return redirect(url_for('gathering.samples'))
    else:
        return render_template('index.html', title='ParasiteWatch Dashboard', \
                               user = user, form = form)

@gathering.route('/samples/edit/<sample_id>', methods=['GET', 'POST'])
@login_required
def samples_edit(sample_id):
    user = current_user
    form = SampleForm()
    sample = Sample.query.filter_by(id=sample_id).first()
    slides = Slide.query.filter((Slide.sample_id==None) | (Slide.sample_id==sample.id))
    if request.method == 'POST':
        if form.validate_on_submit():
            selected_slides = request.form.getlist('slides')
            slide_objects = []
            for slide_id in selected_slides:
                slide_objects.append(Slide.query.filter_by(id=slide_id).first())
            sample.collected_date = form.collected_date.data
            sample.identification = form.identification.data
            sample.country = form.country.data
            sample.city = form.city.data
            sample.district = form.district.data
            sample.number = form.district.data
            sample.slides = slide_objects
            db.session.commit()
            db.session.flush()
            flash('Sample edited successfully', category = 'success')
            return redirect(url_for('gathering.samples'))
        flash('Error sending form data', category = 'danger')
        return redirect(url_for('gathering.samples_edit'))
    if request.method == 'GET':
        return render_template('sample/sample_edit.html', title = 'ParasiteWatch Slides', user = user, form = form, sample = sample, slides = slides)

@gathering.route('/sample/<sample_id>/gallery', methods=['GET'])
@login_required
def samples_gallery(sample_id):
    user = current_user
    sample = Sample.query.filter_by(id=slide_id).first()
    if request.method == 'GET':
        return render_template('sample/sample_gallery.html', title = 'ParasiteWatch Slides', user = user, sample = sample)


@gathering.route('/slides', methods=['GET'])
@login_required
def slides():
    user = current_user
    form = SlideForm()
    slides = Slide.query.all()
    return render_template('slide/slide_index.html', title='ParasiteWatch Samples', user=user, slides=slides, form=form)

@gathering.route('/slides/new', methods=['GET', 'POST'])
@login_required
def slides_new():
    user = current_user
    form = SlideForm()
    images = Image.query.filter_by(slide_id=None)
    if request.method == 'POST':
        if form.validate_on_submit():
            selected_images = request.form.getlist('images')
            image_objects = []
            for image_id in selected_images:
                image_objects.append(Image.query.filter_by(id=image_id).first())
            slide = Slide(creation_date = form.creation_date.data, technique = form.technique.data, images = image_objects)
            db.session.add(slide)
            db.session.commit()
            db.session.flush()
            flash('Slide created successfully', category = 'success')
            return redirect(url_for('gathering.slides'))
        flash('Error sending form data', category = 'danger')
        return redirect(url_for('gathering.slides_new'))
    if request.method == 'GET':
        return render_template('slide/slide_new.html', title = 'ParasiteWatch \
                               Slides', user = user, images = images \
                               ,form = form)


@gathering.route('/slides/destroy/<slide_id>', methods=['POST'])
@login_required
def slides_destroy(slide_id):
    user = current_user
    form = SlideForm()
    if request.method == 'POST':
        slide  = Slide.query.filter_by(id=slide_id).first()
        db.session.delete(slide)
        db.session.commit()
        db.session.flush()
        flash('Slide deleted successfully', category='success')
        return redirect(url_for('gathering.slides'))
    else:
        return render_template('slide/slide_index.html', title='ParasiteWatch Dashboard', user = user, form = form)


@gathering.route('/slides/edit/<slide_id>', methods=['GET', 'POST'])
@login_required
def slides_edit(slide_id):
    user = current_user
    form = SlideForm()
    slide = Slide.query.filter_by(id=slide_id).first()
    images = Image.query.filter((Image.slide_id==None) | (Image.slide_id==slide.id))
    if request.method == 'POST':
        if form.validate_on_submit():
            selected_images = request.form.getlist('images')
            image_objects = []
            for image_id in selected_images:
                image_objects.append(Image.query.filter_by(id=image_id).first())
            slide.creation_date = form.creation_date.data
            slide.technique = form.technique.data
            slide.images = image_objects
            db.session.commit()
            db.session.flush()
            flash('Slide updated successfully', category = 'success')
            return redirect(url_for('gathering.slides'))
        flash('Error sending form data', category = 'danger')
        return redirect(url_for('gathering.slides_new'))
    if request.method == 'GET':
        return render_template('slide/slide_edit.html', title = 'ParasiteWatch Slides', user = user, form = form, slide = slide, images = images)

@gathering.route('/slides/<slide_id>/gallery', methods=['GET'])
@login_required
def slides_gallery(slide_id):
    user = current_user
    slide = Slide.query.filter_by(id=slide_id).first()
    if request.method == 'GET':
        return render_template('slide/slide_gallery.html', title = 'ParasiteWatch Slides', user = user, slide = slide)


@gathering.route('/images', methods=['GET'])
@login_required
def images():
    user = current_user
    images = Image.query.all()
    form = ImageForm()
    return render_template('image/image_index.html', title='ParasiteWatch Images', user=user, images=images, form=form)

@gathering.route('/images/new', methods=['GET', 'POST'])
@login_required
def images_new():
    form = ImageForm()
    user = current_user
    if request.method == 'POST':
        if form.validate_on_submit():
            image = Image(capture_date=form.cdate.data, zoom=form.zoom.data, filename=form.filename.data, url=form.image_url.data)
            db.session.add(image)
            db.session.commit()
            db.session.flush()
            flash('Image created successfully.', category='success')
            return redirect(url_for('gathering.images'))
        flash('Error sending form data.', category='danger')
        return redirect(url_for('gathering.images_new'))
    if request.method == 'GET':
        return render_template('image/image_new.html', title='ParasiteWatch New Image', user=user, form=form)

@gathering.route('/images/edit/<image_id>', methods=['GET', 'POST'])
@login_required
def images_edit(image_id):
    form = ImageForm()
    user = current_user
    image  = Image.query.filter_by(id=image_id).first()
    if request.method == 'POST':
        if form.validate_on_submit():
            image.capture_date = form.cdate.data
            image.zoom = form.zoom.data
            image.filename = form.filename.data
            image.url = form.image_url.data
            db.session.commit()
            db.session.flush()
            flash('Image updated successfully.', category='success')
            return redirect(url_for('gathering.images'))
        flash('Error sending form data.', category='danger')
        return redirect(url_for('gathering.images_edit', image_id = image_id))
    if request.method == 'GET':
        return render_template('image/image_edit.html', title='ParasiteWatch Edit Image', user=user, form=form, image=image)


@gathering.route('/images/destroy/<image_id>', methods=['POST'])
@login_required
def images_destroy(image_id):
    user = current_user
    if request.method == 'POST':
        image  = Image.query.filter_by(id=image_id).first()
        db.session.delete(image)
        db.session.commit()
        db.session.flush()
        flash('Image deleted successfully', category='success')
        return redirect(url_for('gathering.images'))
    else:
        return render_template('index.html', title='ParasiteWatch Dashboard', user=user)
