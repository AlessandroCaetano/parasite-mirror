# Import socketio object and emit method
from .. import socketio

# Video camera thread
from threading import Event
from ..camera import VideoCamera
from threading import Thread

# Import time module to name unique threads
import time

# Thread for camera object
thread = Thread()
STOP_EVENT = Event()

@socketio.on('connected', namespace='/images/snapshot')
def connection_handler(json):
    print('Client Connecting')
    print('received json: {0}'.format(str(json)))

@socketio.on('set_cam', namespace='/images/snapshot')
def set_cam():
    global thread
    global STOP_EVENT
    print('Client Connecting Cam')
    thread = VideoCamera(0, str(time.time), STOP_EVENT)
    if not thread.is_alive() or thread.is_stopped():
        print('Starting Camera Thread')
        thread.start()
    else:
        print('Cam Thread Alive')

@socketio.on('unset_cam', namespace='/images/snapshot')
def unset_cam():
    global thread
    global STOP_EVENT
    if thread.is_alive() or not thread.is_stopped():
        thread.stop()
        thread.__del__()
        print('Cam Thread Cleanned')

@socketio.on('disconnected', namespace='/images/snapshot')
def disconnection_handler(json):
    print('Client Disconnecting')
    print('received json: {0}'.format(str(json)))
    print('Cleaning Cam Threads, If Any')
    if thread.is_alive() or not thread.is_stopped():
        thread.stop()
        thread.__del__()
        print('Cam Thread Cleanned')