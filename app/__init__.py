# Import flask and template operators
from flask import Flask, render_template

# Import flask session manager
from flask_session import Session


# Import SQLAlchemy
from flask_sqlalchemy import SQLAlchemy

# Import Flask Migrate
from flask_migrate import Migrate

# CSRF Protection for all apps
from flask_wtf.csrf import CSRFProtect

# Flask Boostrap
from flask_bootstrap import Bootstrap

# Flask Login for session management
from flask_login import LoginManager

# Flash Marshmallow for api configurations
from flask_marshmallow import Marshmallow

# Flask redis for session store
from flask_redis import FlaskRedis

# Configuraion imports
from instance.config import app_config

# Creating instances for needed objects
lm = LoginManager()

csrf = CSRFProtect()

db = SQLAlchemy()

ma = Marshmallow()

sess = Session()

redis_store = FlaskRedis()

def create_app(config_name):
    # Define the WSGI application object
    app = Flask(__name__, static_url_path='', static_folder='static', instance_relative_config=True)

    # Configurations from file and env
    app.config.from_object(app_config[config_name])
    app.config.from_pyfile('config.py')

    # Cross Site request forgery protection for app
    csrf.init_app(app)

    # Importing bootstrap default templates and static files
    Bootstrap(app)

    # Login manager object now handling app
    lm.init_app(app)

    # Setting default login view
    lm.login_view = "auth.signin"

    # Default login manager message type
    lm.login_message_category = "info"

    # This will initiate the db object with our app configuration
    db.init_app(app)

    # This initiates our api on app and db objects
    ma.init_app(app)

    # Define the migration object which is created
    # from the models and the app
    migrate = Migrate(app,db)

    # Define our session management interface
    sess.init_app(app)

    # Initialize our redis store
    redis_store.init_app(app)

    from . import models

    # Blueprint locations
    from .auth import auth as auth_module
    from .gathering import gathering as gathering_module
    from .api import api as api_module

    # Make api exempt of using csrf protection
    csrf.exempt(api_module)

    # Import a module / component using its blueprint handler variable
    app.register_blueprint(auth_module)
    app.register_blueprint(gathering_module)
    app.register_blueprint(api_module)

    # Routing index page
    @app.route("/")
    def index_page():
        return render_template('index/index.html')

    # Error routing
    @app.errorhandler(404)
    def page_not_found(e):
        return render_template('error-404.html'), 404

    @app.errorhandler(405)
    def page_not_found(e):
        return render_template('error-405.html'), 405

    @app.errorhandler(500)
    def internal_server_error(e):
        return render_template('error-500.html'), 500

    return app