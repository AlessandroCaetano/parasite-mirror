from flask import Blueprint

# Define the blueprint: 'auth', set its url prefix: app.url/auth
auth = Blueprint('auth', __name__, url_prefix='/auth', template_folder='../templates/auth')

from . import views