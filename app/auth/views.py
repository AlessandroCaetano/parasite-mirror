# Import flask dependencies
from flask import render_template, flash, g, redirect, url_for, request

#Import flask login dependencies
from flask_login import login_user, logout_user, current_user, login_required

# Import db configurations
from .. import db

# Import Auth Blueprint
from . import auth

# Import module forms
from .forms import LoginForm

# Import module models (i.e. User)
from ..models import User

# Import SQLAlchemy exceptions
from sqlalchemy.orm.exc import UnmappedInstanceError

# Set the route and accepted methods
@auth.before_request
def before_request():
    g.user = current_user

@auth.route('/signout')
@login_required
def signout():
    try:
        user = current_user
        user.authenticated = False
        db.session.add(user)
        db.session.commit()
        logout_user()
        return redirect(url_for('index_page'))
    except UnmappedInstanceError:
        pass

@auth.route('/signin', methods=['GET', 'POST'])
def signin():
    if g.user is not None and g.user.is_authenticated:
        return redirect(url_for('index_page'))
    # If sign in form is submitted
    form = LoginForm()

    if request.method == 'POST':
        # Verify the sign in form
        if form.validate_on_submit():
            user = User.query.filter_by(username=form.username.data).first()
            if user is not None and user.verify_password(form.password.data):
                user.authenticated = True
                db.session.add(user)
                db.session.commit()
                login_user(user, remember=True)
                flash('Welcome to Parasite Watch', category='success')
                return redirect(url_for('gathering.dashboard'))
            else:
                flash('Wrong username or password', category='danger')
                return redirect(url_for('auth.signin'))
        else:
            return render_template("signin.html", title='Sign In', form=form)
    else:
        return render_template("signin.html", title='Sign In', form=form)

@auth.route('/signup', methods=['GET', 'POST'])
def signup():
    form = LoginForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            if User.query.filter_by(username=form.username.data).first():
                flash('Username already exists', category='danger')
                return redirect(url_for('auth.signup'))
            else:
                new_user = User(username=form.username.data, password=form.password.data)
                db.session.add(new_user)
                db.session.commit()
                flash('User created sucessfuly', category='success')
                return redirect(url_for('auth.signin'))
        else:
            return render_template("signup.html", title='Sign Up', form=form)
    else:
        return render_template("signup.html", title='Sign Up', form=form)