# Import Form and RecaptchaField (optional)
from flask_wtf import FlaskForm#, RecaptchaField

# Import Form elements such as TextField and BooleanField (optional)
from wtforms import StringField, PasswordField, SubmitField

# Import Form validators
from wtforms.validators import DataRequired


# Define the login form (WTForms)
class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired(message='Forgot your username?')])
    password = PasswordField('Password', validators=[DataRequired(message='Must provide a password. ;-)')])
    submit = SubmitField('Sign In')