 # -*- coding: utf-8 -*-
import xmlrunner
import unittest
from datetime import datetime

from app.gathering.models import User, Image, Equipment, Allocation, Slide
from app import app, db


class BasicTest(unittest.TestCase):

	#executed before all tests in one test run (test suite)	
	@classmethod
	def setUpClass(self):

		app.config.from_object('config.TestingConfig')
		self.app = app.test_client()
		self.app.testing = True
		db.drop_all()
		db.create_all()

	#executed after all tests in one test run (test suite)
	@classmethod
	def tearDownClass(self):
		pass

	#executed before each test
	def setUp(self):	
		pass
	
	#executed after each test
	def tearDown(self):
		pass


class UserTest(BasicTest):

	
	def test_create_user(self):
		name = "Jose das Couves"
		email = 'jds@google.com'
		password = 'teste123'
		role = 0

		user = User(name, email, password)
		
		
		print (user.name)
		


		self.assertIsNotNone(user)
		
		db.session.add(user)
		db.session.commit()

		self.assertIsNotNone(user.id)

	
	def test_read_user(self):
		self.test_create_user()
		user = User.query.get(1)
		self.assertIsNotNone(user)
		self.assertEqual(user.name, "Jose das Couves")
		
		user_new = User.query.get(1)
		self.assertIsNotNone(user_new)

	def teste_update_user(self):
		pass
	
	def test_remove_user(self):
		test_create_user()
		user = User.query.get(1)
		self.assertIsNotNone(user.id)

		db.session.delete(user)
		db.session.commit()

		user = User.query.get(1)

		self.assertIsNone(user)

class ImageTest(BasicTest):

	def test_create_image(self):
		date_captured = datetime.today()
		#Zoom values: 100, 400, 
		zoom = 400

		image = Image(date_captured,zoom)
		db.session.add(image)
		db.session.commit()

		self.assertIsNotNone(image.id)
		return image.id

	def test_read_image(self):
		im_id = self.test_create_image()
		image = Image.query.get(im_id)
		self.assertIsNotNone(image)


	def test_update_image(self):
		im_id = self.test_create_image()
		image = Image.query.get(im_id)
		self.assertIsNotNone(image)

		new_zoom = 100

		image.zoom = new_zoom
		db.session.commit()

		new_image = Image.query.get(im_id)
		self.assertIsNotNone(new_image)
		self.assertEqual(new_image.zoom, new_zoom)
	
	def test_delete_image(self):
		im_id = self.test_create_image()
		image = Image.query.get(im_id)
		self.assertIsNotNone(image)

		db.session.delete(image)
		db.session.commit()

		image_new = Image.query.get(im_id)
		self.assertIsNone(image_new)
	

class EquipmentTest(BasicTest):

	def test_create_equipment(self):
		pass

	def test_read_equipment(self):
		pass

	def test_update_equipment(self):
		pass

	def test_delete_equipment(self):
		pass

class AllocationTest(BasicTest):

	def test_create_allocation(self):
		pass

	def test_read_allocation(self):
		pass

	def test_update_allocation(self):
		pass

	def test_delete_allocation(self):
		pass

class SlideTest(BasicTest):


	def test_create_slide(self):
		date_creation = datetime.today()

		technique = 'sedimentação'

		slide = Slide(date_creation, technique)

		db.session.add(slide)
		db.session.commit()

		self.assertIsNotNone(slide.id)
		return slide.id

	def test_read_slide(self):
		slide_id = self.test_create_slide()

		slide = Slide.query.get(slide_id)
		self.assertIsNotNone(slide)

	def test_update_slide(self):
		slide_id = self.test_create_slide()
		slide = Slide.query.get(slide_id)
		self.assertIsNotNone(slide)

		new_technique = 'kto-kt'

		slide.technique = new_technique
		db.session.commit()

		new_slide = Slide.query.get(slide_id)

		self.assertEqual(new_slide.technique, new_technique)		

	def test_delete_slide(self):
		slide_id = self.test_create_slide()
		slide = Slide.query.get(slide_id)
		self.assertIsNotNone(slide)

		db.session.delete(slide)
		db.session.commit()

		new_slide = Slide.query.get(slide_id)
		self.assertIsNone(new_slide)

if __name__ == "__main__":
    unittest.main(
    	testRunner=xmlrunner.XMLTestRunner(output='test_reports')
    	)