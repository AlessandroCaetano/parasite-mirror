#!/bin/bash
export FLASK_CONFIG=development
rm -rf ../__pycache__ ../migrations
sudo mysql -u root < scripts/drop_dev_db.sql
sudo mysql -u root < scripts/create_dev_db.sql
