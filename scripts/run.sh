#!/bin/bash
export FLASK_CONFIG=development
export FLASK_APP=run.py
if [ ! -d instance/ ]
   then
   mkdir -p instance/
   touch instance/config.py
fi
python3 run.py