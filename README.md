# ParasiteWatch

## Overview

This project contains the development files of the Parasite Watch project.
This is part of Parasite Watch project developed in a partnership of COPPE/LENS
with FioCruz and is under the proprerty of LENS.

## Runtime Dependencies

    - Linux System (CentOS/Debian)
    - Python >= 3.4
    - Pip3 >= 9.0
    - NodeJs
    - MariaDB
    - Git
    - SQLite
    - Redis

## Running Development Mode

### Install the required packages:

**APT**

```
sudo apt install python3 python3-pip git mariadb-server libmariadb-dev nodejs build-essential tcl
```
**YUM**

```
sudo yum install python34 python34-setuptools python34-devel git mariadb-server mariadb-devel nodejs
sudo easy_install-3.4 pip
sudo yum groupinstall 'Development Tools'

```
### Install Redis

First, download Redis from this [page](https://redis.io/download).

Extract the tarball, enter the folder and compile files.

```
tar -xaf redis-x.x.x.tar.gz
cd redis-x.x.x/
make
```

Run the test file.

```
make test
```

Install.

```
sudo make install
```

Create a linux service using the script provided with the installation.

```
sudo ./utils/install_server.sh
```

Start the redis-server service.

```
sudo systemctl start redis-server.service
```

### Clone the project:

**SSH**

```
git clone git@www.mrdevops-gitlab.com:ParasiteDev/dev.git
```

**HTTP**

```
git clone https://www.mrdevops-gitlab.com/ParasiteDev/dev.git
```

### Change to project folder:

```
cd dev/
```

### Export FLASK_CONFIG and FLASK_APP environment variables:

```
export FLASK_CONFIG=development
```
```
export FLASK_APP=run.py
```

### Create a instance configuration file:

```
mkdir instance/
```
```
touch instance/config.py
```

### Create a variable for SECRET_KEY on instance/config.py:

```
SECRET_KEY='PUT SOMETHING HERE'
```

### Start MariaDB service:

```
sudo systemctl start mysql.service
```

### Create the database with the scripts:
```
sudo mysql -u root < scripts/create_parasite_user.sql
sudo mysql -u root < scripts/create_dev_db.sql
```

### Migrate de database:
```
flask db init
flask db migrate
flask db upgrade
```

### Install virtualenv:
```
pip install virtualenv
```

### Create a virtualenv:
```
virtualenv -p /usr/bin/python3.6 venv
```

### Activate your virtualenv:
```
source venv/bin/activate
```

### Install python dependencies:
```
pip install -r requirements.txt
```

### Install JavaScript and JQuery dependencies:
```
cd app/static
npm install 
```

### Run the web server:
```
./scripts/run.sh
```
